from airflow.models import DAG
from airflow.providers.docker.operators.docker import DockerOperator
from datetime import datetime

default_args = {
    'owner': 'volodin.dd',
    'email': ['my_awesom_name@example.com'],
    'email_on_failure': False
}

with DAG(
    dag_id='my_cool_r_dag',
    default_args=default_args,
    start_date=datetime(2023, 2, 20, 11, 40),
    schedule_interval='0 */2 * * *',
    catchup=False,
    tags=['R', 'docker', 'training']
) as dag:

    task = DockerOperator(
        api_version='auto',
        command='Rscript /home/my_cool_r_project/R/hello.R',
        image='registry.gitlab.com/dmitry_volodin/my_cool_r_project',
        docker_conn_id='gitlab',
        task_id='hello',
        force_pull=True,
        auto_remove=True,
        dag=dag
    )